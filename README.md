# Digi; Interface for the Open Celtic Dictionary
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![Discord](https://img.shields.io/discord/718008955040956456.svg?label=&logo=discord&logoColor=ffffff&color=7389D8&labelColor=6A7EC2)](https://discord.gg/UzaFmfV)

Digi is the multi-interface bot for the Open Celtic Dictionary. Currently it works on Discord and Telegram.
It is used to serve content from the Open Celtic Dictionary API in chats and similar.

## Main Repository 

The main repository for the Open Celtic Dictionary can be found here: [Gitlab repository](https://gitlab.com/prvInSpace/open-celtic-dictionary-api-server)

## Maintainer

Digi is maintained by Preben Vangberg &lt;prv@aber.ac.uk&gt;
