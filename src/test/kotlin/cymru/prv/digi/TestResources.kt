package cymru.prv.digi

import cymru.prv.digi.common.Language
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

/**
 * Tests to ensure that resources are loaded properly
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class TestResources {

    @Test
    fun `is able to load internal resources`(){
        assertEquals("Website", Resources.getString(Language.EN, "website"))
        assertEquals("Gwefan", Resources.getString(Language.CY, "website"))
    }


}