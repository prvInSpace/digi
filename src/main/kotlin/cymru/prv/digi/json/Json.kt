package cymru.prv.digi.json

import cymru.prv.dictionary.common.json.JsonSerializable
import org.json.JSONArray
import org.json.JSONObject

/**
 * Helper functions for JSON
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */

/**
 * Takes the key and converts that field in
 * the JSON object and converts it to a list
 * of strings.
 *
 * Supports single strings, list of strings,
 * and lists of references.
 */
fun JSONObject.toStringArray(key: String): List<String> {
    if(!this.has(key))
        return listOf()
    val obj = this.get(key)

    if(obj is JSONArray) {
        return obj.map {
            when (it) {
                is JSONObject -> it.getString("value")
                else -> it as String
            }
        }.toList()
    }
    if(obj is String)
        return listOf(obj)

    return listOf()
}


/**
 * Converts a map of strings and JsonSerializable objects
 * to a JSON object
 */
fun Map<String, JsonSerializable>.toJsonObject():JSONObject {
    val obj = JSONObject()
    for(key in keys)
        obj.put(key, getValue(key).toJson())
    return obj
}


/**
 * Converts a list of JsonSerializable objects
 * to a JSON array
 */
fun List<JsonSerializable>.toJsonArray(): JSONArray {
    val array = JSONArray()
    for(value: JsonSerializable in this.listIterator())
        array.put(value.toJson())
    return array;
}