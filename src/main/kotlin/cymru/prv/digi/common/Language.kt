package cymru.prv.digi.common

/**
 * Represents available
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
enum class Language(val languageName: String) {
    EN("english"),
    BR("breton"),
    CY("welsh"),
	DE("german"),
	ES("spanish"),
    KW("cornish"),
    GA("irish"),
    GD("scottish_gaelic"),
    GV("manx"),
    FR("french"),
    NO("norwegian"),
    NN("norwegian")
}
