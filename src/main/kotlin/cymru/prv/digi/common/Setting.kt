package cymru.prv.digi.common

/**
 * @author Preben Vangberg
 */
enum class Setting(val keyName: String, val validValues: List<String>) {

    DefaultLanguage("lang", Language.values().map { it.toString() }.toList()),
    DefaultOutputLanguage("outlang", Language.values().map { it.toString() }.toList());

    companion object {

        private val fieldNameMap = values().associateBy { it.keyName }

        fun fromEvent(event: CommandEvent): Setting? {
            if (event.isEmpty()) {
                event.sendError(event.getString("missing_key"))
                return null
            }

            val name = event.pop().toLowerCase()
            if (fieldNameMap.containsKey(name))
                return fieldNameMap[name]

            event.sendError("invalid_setting_key")
            return null
        }
    }
}
