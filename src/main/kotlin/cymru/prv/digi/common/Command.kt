package cymru.prv.digi.common

import cymru.prv.digi.interfaces.discord.DiscordCommandEvent
import net.dv8tion.jda.api.EmbedBuilder
import org.json.JSONObject
import java.awt.Color
import java.lang.RuntimeException
import java.util.function.Consumer
import kotlin.reflect.KClass

class Command private constructor(builder: Builder) {


    private val commands = builder.commands
    val shortAbout = builder.shortAbout
    private val handlers = builder.handlers
    private val usage = builder.usage

    fun handle(event: CommandEvent) {
        handlers[event::class]?.accept(event)
    }

    data class CommandForm(val lang: Language, val command: Command)

    val forms: List<Pair<String, CommandForm>> = commands.strings.keys.map {
        (commands.strings[it] ?: throw RuntimeException("Unable to fetch command")) to CommandForm(it, this)
    }.toList()

    fun getFormForLanguage(lang: Language): String {
        return commands.getString(lang)
    }

    fun findCommandForm(command: String): CommandForm? {
        commands.strings.keys.forEach {
            if(commands.getString(it) == command)
            return CommandForm(it, this)
        }
        return null
    }

    class Builder(command: String, handler: Consumer<CommandEvent>) {

        private val obj = JSONObject(javaClass.getResource("/commands/$command.json").readText())


        val commands = Translation(obj.getJSONObject("forms"))
        val shortAbout = Translation(obj.getJSONObject("about_short"))
        val usage = if(obj.has("usage")) Translation(obj.getJSONObject("usage")) else null

        val handlers: MutableMap<KClass<out CommandEvent>, Consumer<CommandEvent>> = mutableMapOf(
            CommandEvent::class to handler,
            DiscordCommandEvent::class to handler
        )

        fun build(): Command = Command(this)

        fun <T: CommandEvent> setOverride(eventClass: KClass<T>, handler: Consumer<T>): Builder {
            handlers[eventClass] = handler as Consumer<CommandEvent>
            return this
        }
    }

    fun printInformation(event: DiscordCommandEvent) {
        val builder = EmbedBuilder()
        builder.setColor(Color.CYAN)
        builder.setTitle("${event.getString("command")}: ${getFormForLanguage(Language.EN)}")

        builder.setDescription(shortAbout.getString(event.outputLanguage))

        if(usage != null)
            builder.addField("Usage", usage.getString(event.outputLanguage), false)

        if(commands.strings.size > 1)
            builder.addField("Forms", commands.strings
                .map { "${event.getString(it.key.languageName)} : ${it.value}"  }
                .joinToString(separator = "\n"), false)

        event.sendMessage(builder.build())
    }

    fun printInformation(event: CommandEvent) {
        val builder = StringBuilder()
        builder.append(shortAbout.getString(event.outputLanguage))

        if(usage != null)
            builder.append("\nUsage\n${usage.getString(event.outputLanguage)}\n")

        if(commands.strings.size > 1)
            builder.append("Forms:\n" + commands.strings
                .map { "${event.getString(it.key.languageName)} : ${it.value}"  }
                .joinToString(separator = "\n"))

       event.sendMessage(
           event.formatHeader("${getFormForLanguage(Language.EN)}\n")
               + event.formatMonospace(builder.toString()))
    }
}



