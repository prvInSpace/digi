package cymru.prv.digi.common

import cymru.prv.digi.Digi
import cymru.prv.digi.Resources
import cymru.prv.digi.plugins.Plugin
import java.util.*

/**
 * @author Preben Vangberg
 */
abstract class CommandEvent(command: String) {

    var outputLanguage = Language.EN

    internal val queue: Queue<String> = LinkedList(command.trim().split("\\s+".toRegex()).filter { it.isNotEmpty() })

    abstract fun getInterfacePlugin(): Plugin
    abstract fun sendMessage(content: String)
    open fun sendError(content: String) = sendMessage(content)

    fun handle(){

        if(queue.isEmpty()){
            Digi.commands.getValue("help").command.handle(this)
            return
        }

        outputLanguage = getDefaultOutputLanguage()

        // If the first argument starts with a !
        // try to parse it and set the output language
        if(queue.peek().startsWith("!")){
            val value = pop().substring(1).toUpperCase()
            try {
                outputLanguage = Language.valueOf(value)
            }
            catch (e: Exception){
                sendError(getString("unknown_language").format(value))
                return
            }
        }

        val firstCommand = pop()
        val form = fetchCommandForm(firstCommand)
        if(form != null){
            if(form.lang != Language.EN && outputLanguage == Language.EN)
                outputLanguage = form.lang
            try {
                form.command.handle(this)
            }
            catch (e: Exception){
                e.printStackTrace()
                sendError("Unknown error occurred. If this keeps occurring, please contact the team.")
            }
        }
        else {
            sendError(getString("unknown_command").format(firstCommand))
        }

    }

    fun fetchCommandForm(command: String): Command.CommandForm? {
        if(command in Digi.commands)
            return Digi.commands.getValue(command)
        return getInterfacePlugin().findCommandForm(command)
    }

    fun getString(key: String): String = Resources.getString(outputLanguage, key)

    fun pop(): String = queue.poll()
    fun peek(): String? = queue.peek()
    fun isEmpty(): Boolean = queue.isEmpty()
    fun size(): Int = queue.size

    // Formatting functions
    open fun formatHeader(content: String): String { return content }
    open fun formatMonospace(content: String): String { return content }


    // Settings related functions
    open fun getDefaultLanguage(): Language = Language.EN
    open fun getDefaultOutputLanguage(): Language = Language.EN
}