package cymru.prv.digi.common

import org.json.JSONObject

class Translation(val strings: Map<Language, String>){

    constructor(obj: JSONObject): this(obj.keySet().associate
        { Language.valueOf(it.toUpperCase()) to obj.getString(it) })


    fun getString(lang: Language): String {
        if(lang in strings)
            return strings.getValue(lang)
        return strings.getValue(Language.EN)
    }

}