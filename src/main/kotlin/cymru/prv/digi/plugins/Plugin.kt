package cymru.prv.digi.plugins

import cymru.prv.digi.common.Command

/**
 * @author Preben Vangberg
 */
abstract class Plugin internal constructor(val name: String) {

    abstract fun getCommands(): List<Command>

    fun findCommandForm(firstCommand: String): Command.CommandForm? {
        for(command in getCommands()) {
            val form = command.findCommandForm(firstCommand)
            if( form != null)
                return form
        }
        return null
    }

}