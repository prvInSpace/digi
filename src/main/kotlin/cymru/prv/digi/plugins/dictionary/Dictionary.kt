package cymru.prv.digi.plugins.dictionary

import cymru.prv.dictionary.common.WordType
import cymru.prv.digi.common.Command
import cymru.prv.digi.common.CommandEvent
import cymru.prv.digi.common.Language
import cymru.prv.digi.plugins.Plugin
import org.json.JSONObject
import java.net.URL
import java.net.URLEncoder
import java.util.function.BiConsumer

/**
 * The plugin for the Open Celtic Dictionary
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
object Dictionary: Plugin("open_celtic_dictionary") {

    private val supportedLanguages = setOf(
        Language.BR,
        Language.CY,
        Language.GA,
        Language.GD,
        Language.GV,
        Language.KW
    )

    private val commands = listOf(
        Command.Builder("adj") { fetchWord(it, WordType.adjective) }.build(),
        Command.Builder("adp") { fetchWord(it, WordType.adposition) }.build(),
        Command.Builder("noun") { fetchWord(it, WordType.noun) }.build(),
        Command.Builder("tense") { fetchConjugation(it) }.build(),
        Command.Builder("verb") { fetchWord(it, WordType.verb) }.build(),

        // The word type for search is ignored by the API
        Command.Builder("search") { fetchWord(it, WordType.verb, true) }.build(),

        Command.Builder("trans", this::translationsCommandHandler).build(),
        Command.Builder("link", this::fetchLink).build()
    )

    override fun getCommands(): List<Command> {
        return commands
    }

    private fun fetchWord(event: CommandEvent, type: WordType, search: Boolean = false){

        val lang = fetchLanguageFromEvent(event) ?: return
        val word = URLEncoder.encode(fetchWordFromEvent(event) ?: return, "utf-8")
        val url = "https://api.prv.cymru/api/dictionary?lang=${lang.toString().toLowerCase()}&word=$word&type=$type&search=$search"

        fetchAndHandleRequest(event, url, this::handleResponse)
    }

    private fun fetchJsonFromAPI(event: CommandEvent, url: String): JSONObject? {
        val obj = JSONObject(URL(url).readText())
        if(!obj.getBoolean("success")) {
            event.sendError(obj.getString("message"))
            return null
        }
        return obj
    }

    private fun fetchAndHandleRequest(event: CommandEvent, url: String, handler: BiConsumer<CommandEvent, JSONObject>){
        val obj = fetchJsonFromAPI(event, url) ?: return
        handler.accept(event, obj)
    }

    private fun translationsCommandHandler(event: CommandEvent){
        val word: String = event.queue.joinToString(separator = " ")
        val encodedWord = URLEncoder.encode(word, "utf-8")
        val url = "https://api.prv.cymru/api/dictionary/translate?word=$encodedWord"
        fetchAndHandleRequest(event, url, this::handleTranslations)
    }

    private fun handleTranslations(event: CommandEvent, obj: JSONObject){
        val array = obj.getJSONArray("results")
        if(array.length() == 0)
            event.sendError(event.getString("no_translations"))
        else
            event.sendMessage(event.formatMonospace(fetchTranslations(event, array)))
    }

    private fun handleResponse(event: CommandEvent, obj: JSONObject){

        val builder = StringBuilder()
        builder.append(event.formatHeader(event.getString("open_celtic_dictionary")))
            .append("\n")
            .append(event.getString("dictionary_search_body"))
            .append("\n");

        val array = obj.getJSONArray("results");
        array.forEach {
            it as JSONObject
            builder.append(event.formatMonospace(
                DictionaryWord(it).toString(event)
            ))
            if(it.has("id") && it.getInt("id") >= 0)
                builder.append("https://digi.prv.cymru?dict=${it.getString("lang")}&id=${it.getInt("id")}")
        }
        event.sendMessage(builder.toString())
    }

    private fun fetchLink(event: CommandEvent){
        val lang = fetchLanguageFromEvent(event) ?: return
        val word = URLEncoder.encode(fetchWordFromEvent(event) ?: return, "utf-8")
        val type = fetchTypeFromEvent(event) ?: return

        event.sendMessage("https://digi.prv.cymru?dict=${lang.toString().toLowerCase()}&word=$word&type=$type&search=false")
    }

    private fun fetchConjugation(event: CommandEvent){
        val obj = fetchJsonFromAPI(event, generateApiUrl(
            URLEncoder.encode(fetchWordFromEvent(event) ?: return, "utf-8"),
            fetchLanguageFromEvent(event) ?: return,
            WordType.verb
        )) ?: return
        val conjugation = fetchWordFromEvent(event) ?: return

        val array = obj.getJSONArray("results")
        val word = array.getJSONObject(0)
        val conjugations = word.getJSONObject("conjugations")
        if(conjugations.has(conjugation))
            event.sendMessage(event.formatMonospace(fetchInflection(event, conjugations.getJSONObject(conjugation), "tense_$conjugation")))
        else
            event.sendError("Word doesn't have '$conjugation'")
    }

    private fun fetchTypeFromEvent(event: CommandEvent): WordType? {
        if(event.isEmpty()){
            event.sendError("missing_value")
            return null
        }

        val string = event.pop()
        return try {
            WordType.valueOf(string)
        } catch (e: Exception){
            event.sendError("Unknown type '$string'")
            null
        }
    }

    private fun fetchWordFromEvent(event: CommandEvent): String? {
        if(!event.isEmpty())
            return event.pop()
        event.sendError(event.getString("missing_value"))
        return null
    }

    private fun fetchLanguageFromEvent(event: CommandEvent): Language? {

        val default = event.getDefaultLanguage()
        val lang = when {
            !event.isEmpty() && event.peek()!!.startsWith("!") -> {
                val langString = event.pop().substring(1).toUpperCase()
                try {
                    Language.valueOf(langString)
                }
                catch (e: Exception){
                    event.sendError(event.getString("unknown_language").format(langString))
                    return null
                }
            }
            default in supportedLanguages -> default
            else -> Language.CY
        }

        if(lang !in supportedLanguages){
            event.sendError(event.getString("unsupported_language").format(lang))
            return null
        }

        return lang
    }

    private fun generateApiUrl(word: String, lang: Language, type: WordType, search: Boolean = false): String{
        return "https://api.prv.cymru/api/dictionary?lang=${lang.toString().toLowerCase()}&word=$word&type=$type&search=$search"
    }

}

