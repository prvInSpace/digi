package cymru.prv.digi.plugins.dictionary

import cymru.prv.dictionary.common.WordType
import cymru.prv.digi.common.CommandEvent
import cymru.prv.digi.common.Language
import org.json.JSONObject

/**
 * Represents a single word from the
 * results part of the API call
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class DictionaryWord(val obj: JSONObject) {

    /**
     * The language of the word
     */
    val lang = Language.valueOf(obj.getString("lang").toUpperCase())
    val type = WordType.valueOf(obj.getString("type"))

    /**
     * Converts the object to its string form.
     * Requires event for translation purposes
     */
    fun toString(event: CommandEvent): String {
        val builder = StringBuilder()
        builder.append(fetchBasicInformation(event))
            .append(fetchTranslations(event))
            .append(fetchMutations(event))

        if(type == WordType.adposition && obj.has("inflections"))
            builder.append(fetchInflection(event, obj.getJSONObject("inflections"), "inflections"))

        if(type == WordType.verb && obj.has("conjugations")){
            val conjugations = obj.getJSONObject("conjugations")
            Tenses.values().forEach {
                if (conjugations.has(it.name))
                    builder.append("\n").append(fetchInflection(event, conjugations.getJSONObject(it.name), "tense_${it.name}"))
            }
        }

        return builder.toString()
    }

}