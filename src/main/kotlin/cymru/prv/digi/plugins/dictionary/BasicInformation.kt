package cymru.prv.digi.plugins.dictionary

import cymru.prv.digi.common.CommandEvent
import cymru.prv.digi.json.toStringArray
import org.json.JSONObject

/**
 * Helps print all of the basic information
 * that is available for all file types +
 * some of the inflections that fits
 * better in the header bit of information
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */

var idLists = listOf(
    "versions",
    "related",
    "synonyms",
    "antonyms"
)

var inflections = listOf(
    "plural",
    "verbalNoun",

    // Adjective stuff
    "comparative",
    "superlative",
    "exclamative",
    "equative"
)

fun DictionaryWord.fetchBasicInformation(event: CommandEvent): String {
    val builder = StringBuilder()

    builder.append("${event.getString("word")}: ${obj.getString("normalForm").capitalize()}\n")
    builder.append("${event.getString("type")}: ${event.getString(obj.getString("type"))}\n")

    builder.append("${event.getString("status")}: ")
    if(obj.getBoolean("confirmed"))
        builder.append(event.getString("confirmed"))
    else
        builder.append(event.getString("generated"))
    builder.append("\n")

    // Print ID lists if exist
    builder.append(idLists.printAllLists(event, obj))

    // Print certain inflections
    if(obj.has("inflections"))
        builder.append(inflections.printAllLists(event, obj.getJSONObject("inflections")))

    if (obj.has("etymology"))
        builder.append("\n${event.getString("etymology")}: ${obj.getString("etymology")}\n")
    if (obj.has("notes"))
        builder.append("\n${event.getString("notes")}: ${obj.getString("notes")}\n")

    return builder.toString()
}


/**
 * Prints all of the keys from the list
 * that is present in the object and
 * prints them as a new section
 */
fun List<String>.printAllLists(event: CommandEvent, obj: JSONObject): String {
    val present = this intersect obj.keySet()
    if(present.isEmpty())
        return "";

    val builder = StringBuilder()
    builder.append("\n")
    for(key in present)
        builder.append("${event.getString(key)}: ${obj.toStringArray(key).joinToString(separator = ", ")}\n")
    return builder.toString()
}

