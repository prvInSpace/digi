package cymru.prv.digi.plugins.dictionary

/**
 * @author Preben Vangberg
 */
enum class Tenses {
    present,
    present_short,
    present_long,
    present_future,
    present_independent,
    present_dependent,
    present_affirmative,
    present_interrogative,
    present_negative,
    present_situative,
    present_habitual,
    past,
    preterite,
    past_independent,
    past_dependent,
    past_habitual,
    imperfect,
    imperfect_short,
    imperfect_long,
    imperfect_situative,
    imperfect_habitual,
    imperfect_affirmative,
    imperfect_negative,
    imperfect_interrogative,
    future,
    future_habitual,
    future_independent,
    future_dependent,
    future_relative,
    conditional,
    conditional_present,
    conditional_imperfect,
    conditional_independent,
    conditional_dependent,
    subjunctive_present_future,
    subjunctive_imperfect,
    imperative
}
