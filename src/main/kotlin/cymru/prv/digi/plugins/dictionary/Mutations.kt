package cymru.prv.digi.plugins.dictionary

import cymru.prv.common.cli.TextTable
import cymru.prv.digi.common.CommandEvent
import cymru.prv.digi.common.Language

/**
 * A file made to handle transforming
 * mutations into a table
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */

var languageMutationsMap = mapOf(
    Language.CY to listOf(
        "init",
        "soft",
        "nasal",
        "aspirate"
    ),
    Language.BR to listOf(
        "init",
        "soft",
        "spirant",
        "hard",
        "mixed"
    )
)

fun DictionaryWord.fetchMutations(event: CommandEvent): String {
    if(!obj.has("mutations"))
        return "";

    if(this.lang !in languageMutationsMap.keys)
        return "";


    val mutations = obj.getJSONObject("mutations")
    val mutationTypes = languageMutationsMap.getValue(this.lang)

    val words = mutationTypes.map { mutations.getString(it) }.toTypedArray()
    if( words.count { it.isNotEmpty() } < 2)
        return ""

    val table = TextTable("l" + " | l".repeat(mutationTypes.size-1))

    table.addRow(*mutationTypes.map { event.getString("mutation_$it") }.toTypedArray())
    table.addSplit()
    table.addRow(*mutationTypes.map { mutations.getString(it) }.toTypedArray())


    return "\n${event.getString("mutations")}:\n${table.build()}"
}