package cymru.prv.digi.plugins.dictionary

import cymru.prv.digi.common.CommandEvent
import cymru.prv.digi.common.Language
import cymru.prv.digi.json.toStringArray
import org.json.JSONArray
import org.json.JSONObject

/**
 * A file designed to handle converting
 * translations into a printable form
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */

data class TranslationUnit(val langName: String){
    val translations = mutableSetOf<String>()
}

fun DictionaryWord.fetchTranslations(event: CommandEvent): String {
    if(!obj.has("translations"))
        return "";
    val array = obj.getJSONArray("translations")
    return fetchTranslations(event, array)
}

fun fetchTranslations(event: CommandEvent, array: JSONArray): String {

    val map = mutableMapOf<Language, TranslationUnit>()

    array.map { it as JSONObject}.forEach {
        it.keys().forEach { lang ->
            val language = Language.valueOf(lang.toUpperCase())
            if(!map.containsKey(language))
                map[language] = TranslationUnit(event.getString(language.languageName))
            map.getValue(language).translations.addAll(it.toStringArray(lang))
        }
    }

    if(map.isEmpty())
        return "";

    val builder = StringBuilder("\n")

    map.values.sortedBy { it.langName }.forEach {
        builder.append("%s: %s\n".format(it.langName, it.translations.joinToString(separator = ", ")))
    }

    return builder.toString();
}