package cymru.prv.digi.plugins.dictionary

import cymru.prv.common.cli.TextTable
import cymru.prv.digi.common.CommandEvent
import cymru.prv.digi.json.toStringArray
import org.json.JSONObject

/**
 * A file containing the logic of converting
 * an inflection over to a table. Support both
 * tenses and normal inflections
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */

/**
 * An enum representing all of the types of inflections
 * that the class support. They are separated into each row
 * with the singular and plural key for each row
 */
private enum class Inflections(val singular: String, val plural: String) {
    first("singFirst", "plurFirst"),
    second("singSecond", "plurSecond"),
    third("singThird", "plurThird"),
    third_masculine("singThirdMasc", ""),
    third_feminine("singThirdMasc", ""),
    impersonal("impersonal", ""),
    analytic("analytic", "")
}


/**
 * Converts the JSON object that was provided into an inflection
 * table.
 *
 * @param event The event. Used for translations
 * @param obj The JSON object with the inflections
 * @param name The key of the name of the table
 */
fun fetchInflection(event: CommandEvent, obj: JSONObject, name: String): String{

    data class Pair(val singular: List<String>, val plural: List<String>)

    val map = Inflections.values().filter {
        obj.has(it.singular) || obj.has(it.plural)
    }.associateWith { infl ->
        val singular = obj.toStringArray(infl.singular)
        val plural = obj.toStringArray(infl.plural)
        Pair(singular, plural)
    }

    val table = TextTable("l | l | l")
    table.setColMaxWidth(0, 20)
    table.addRow(event.getString(name), event.getString("singular"), event.getString("plural"))
    table.addSplit()

    Inflections.values().forEach { infl ->
        if(map.containsKey(infl)) {
            val values = map.getValue(infl)
            if(values.singular.size + values.plural.size > 0)
                table.addRow(event.getString(infl.name),
                    values.singular.joinToString(separator = ", "),
                    values.plural.joinToString(separator = ", "))
        }
    }

    return "\n" + table.build()
}