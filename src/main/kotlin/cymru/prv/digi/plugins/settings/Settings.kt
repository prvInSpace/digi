package cymru.prv.digi.plugins.settings

import cymru.prv.dictionary.common.json.JsonSerializable
import cymru.prv.digi.common.Setting
import org.json.JSONObject

/**
 * Represents a set of settings for one unique
 * person, server, or whatever.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class Settings(obj: JSONObject): JsonSerializable {

    private val map = obj.toKeyValueMap()

    override fun toJson(): JSONObject {
        return JSONObject(map)
    }

    fun setKey(setting: Setting, value: String){
        map[setting.keyName] = value
    }

    fun unsetKey(setting: Setting){
        map.remove(setting.keyName)
    }

    fun isEmpty() = map.isEmpty()

    fun listKeys(): String {
        val builder = StringBuilder()
        map.keys.forEach {
            builder.append("$it: ${map[it]}\n")
        }
        return builder.toString()
    }

    fun getValue(setting: Setting): String? {
        return map[setting.keyName]
    }

}

private fun JSONObject.toKeyValueMap(): MutableMap<String, String> {
    return keys().asSequence()
        .associateWith { getString(it) }
        .toMutableMap()
}
