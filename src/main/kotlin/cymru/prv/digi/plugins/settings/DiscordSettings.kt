package cymru.prv.digi.plugins

import cymru.prv.digi.common.Command
import cymru.prv.digi.common.CommandEvent
import cymru.prv.digi.interfaces.discord.DiscordCommandEvent
import cymru.prv.digi.json.toJsonObject
import cymru.prv.digi.common.Setting
import cymru.prv.digi.plugins.settings.Settings
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import org.json.JSONObject
import java.awt.Color
import java.io.File
import java.util.function.Function

/**
 * @author Preben Vangberg
 * @since 1.0.0
 */

private enum class Receiver (private val handler: Function<MessageReceivedEvent, String>,
                             private val nameHandler: Function<MessageReceivedEvent, String>) {

    SERVER ({ it.guild.id }, { it.guild.name}),
    CHANNEL({ it.channel.id }, { it.channel.name}),
    USER ({ it.author.id }, { it.member?.effectiveName ?: it.author.name });

    fun getName(event: DiscordCommandEvent) = nameHandler.apply(event.event)
    fun getId(event: MessageReceivedEvent) = handler.apply(event)
    fun getId(event: DiscordCommandEvent) = handler.apply(event.event)
}

object DiscordSettings: Plugin("Discord") {

    private val settings = fetchKeyValueMaps()

    private val commands = listOf(
        Command.Builder("discord_set", this::invalidInterface)
            .setOverride(DiscordCommandEvent::class, this::setKey)
            .build(),
        Command.Builder("discord_unset", this::invalidInterface)
            .setOverride(DiscordCommandEvent::class, this::unsetKey)
            .build(),
        Command.Builder("discord_keys", this::invalidInterface)
            .setOverride(DiscordCommandEvent::class, this::listKeys)
            .build()
    )

    override fun getCommands(): List<Command> {
        return commands
    }

    fun getSetting(event: MessageReceivedEvent, setting: Setting): String? {
        val orderOfPrecedent = listOf(Receiver.USER, Receiver.CHANNEL, Receiver.SERVER)
        for(receiver in orderOfPrecedent){
            val id = receiver.getId(event)
            if(settings.containsKey(id)){
                val receiverSettings = settings.getValue(id)
                val key = receiverSettings.getValue(setting)
                if(key != null)
                    return key
            }
        }
        return null
    }

    private fun setKey(event: DiscordCommandEvent){
        val receiver = getReceiver(event) ?: return
        val targetSetting: Setting = Setting.fromEvent(event) ?: return
        val setting = settings.getOrPut(receiver.getId(event)) { Settings(JSONObject()) }

        if(event.isEmpty()) {
            event.sendError(event.getString("missing_value"))
        }

        val value = event.pop()
        if(value in targetSetting.validValues){
            setting.setKey(targetSetting, value)
            saveKeys()

            val builder = EmbedBuilder()
            builder.setTitle("Key-set successful")
            builder.setDescription("$targetSetting set to $value for ${receiver.getName(event)}")
            builder.setColor(Color.GREEN)
            event.sendMessage(builder.build())
        }
        else {
            val builder = EmbedBuilder()
            builder.setTitle("Invalid value for key")
            builder.setDescription("The key was not set because the value was not valid.")
            builder.setColor(Color.RED)
            builder.addField("Setting", targetSetting.name, false)
            builder.addField("Valid values", targetSetting.validValues.joinToString(separator = ", "), false)
            event.sendMessage(builder.build())
        }
    }

    /**
     * Tries to unset a specific key from the receiver
     */
    private fun unsetKey(event: DiscordCommandEvent){
        val receiver = getReceiver(event) ?: return
        val targetSetting: Setting = Setting.fromEvent(event) ?: return
        val setting = settings[receiver.getId(event)]

        if(setting != null) {
            setting.unsetKey(targetSetting)
            if (setting.isEmpty())
                settings.remove(receiver.getId(event))
            saveKeys()
        }

        val builder = EmbedBuilder()
        builder.setTitle(event.getString("key_removal_successful"))
        builder.setDescription("Succefully removed $targetSetting from ${receiver.getName(event)}")
        builder.setColor(Color.GREEN)
        event.sendMessage(builder.build())
    }

    private fun listKeys(event: DiscordCommandEvent){

        val builder = EmbedBuilder()

        for(receiver in Receiver.values()){
            if(settings.containsKey(receiver.getId(event)))
                builder.addField(
                    event.getString("keys_${receiver.toString().toLowerCase()}"),
                    settings.getValue(receiver.getId(event)).listKeys(),
                    false)
        }

        val color = if(builder.isEmpty) {
            builder.setDescription(event.getString("no_active_keys"))
            Color.RED
        }
        else {
            Color.GREEN
        }

        builder.setTitle("Active keys")
        builder.setColor(color)

        event.sendMessage(builder.build())
    }

    private fun getReceiver(event: DiscordCommandEvent): Receiver? {

        if(event.isEmpty()) {
            event.sendError("Missing parameter 'receiver'. Please use either server, channel, or user")
            return null
        }

        val parameter = event.pop()
        return try {
            Receiver.valueOf(parameter.toUpperCase())
        }
        catch (e: Exception) {
            event.sendError("Invalid parameter '$parameter'. Please use either server, channel, or user")
            null
        }
    }

    /**
     * A handler in case the wrong interface is able
     * to call the functions in the plugin
     */
    private fun invalidInterface(event: CommandEvent) {
        event.sendError("Invalid interface. This function is only available for Discord")
    }

    private fun saveKeys(){
        File("res/settings.json").writeText(settings.toJsonObject().toString())
    }
}

private fun fetchKeyValueMaps(): MutableMap<String, Settings> {
    val obj = JSONObject(File("res/settings.json").readText())
    return obj.keys().asSequence().associate {
        it as String to Settings(obj.getJSONObject(it))
    }.toMutableMap()
}
