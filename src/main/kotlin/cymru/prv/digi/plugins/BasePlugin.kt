package cymru.prv.digi.plugins

import cymru.prv.digi.Digi
import cymru.prv.digi.common.Command
import cymru.prv.digi.common.CommandEvent
import cymru.prv.digi.interfaces.discord.DiscordCommandEvent
import net.dv8tion.jda.api.EmbedBuilder
import java.awt.Color

/**
 * Contains all of the main commands for Digi
 * that has nothing to do with any of the other
 * plugins that Digi might have
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
object BasePlugin: Plugin("general") {

    private val baseColor = Color(101, 83, 203)

    /**
     * The commands that are available for this
     * plugin
     */
    private val commands = listOf(
        Command.Builder("about", this::printAbout)
            .setOverride(DiscordCommandEvent::class, this::printDiscordAbout)
            .build(),
        Command.Builder("version", this::printVersion)
            .setOverride(DiscordCommandEvent::class, this::printVersion)
            .build(),
        Command.Builder("help", this::printHelp)
            .setOverride(DiscordCommandEvent::class, this::printHelp)
            .build(),
        Command.Builder("list", this::printList)
            .build()
    )


    /**
     * Returns the list of commands
     * for the plugin
     */
    override fun getCommands(): List<Command> {
        return commands
    }

    /**
     * Override for Discord event.
     * Prints the information about the bot as an
     * embedded message
     */
    private fun printDiscordAbout(event: DiscordCommandEvent){
        val builder = EmbedBuilder()
        builder.setTitle(event.getString("about_digi"))
        builder.setAuthor("Digi", "https://digi.prv.cymru", "https://digi.prv.cymru/digi_v2.png")
        builder.setColor(baseColor)
        builder.setThumbnail("https://digi.prv.cymru/digi_v2.png")
        builder.setDescription(event.getString("about_description"))
        builder.addField(event.getString("website"), "https://digi.prv.cymru", false)
        builder.addField(event.getString("repository"), "https://gitlab.com/prvInSpace/digi", false)
        builder.addField(event.getString("twitter"), "https://twitter.com/", false)
        builder.addField("Discord", "https://discord.gg/UzaFmfV", false)
        event.sendMessage(builder.build())
    }

    private fun printAbout(event: CommandEvent){
        event.sendMessage(event.getString("about_digi"))
    }

    private fun printVersion(event: DiscordCommandEvent){
        val builder = EmbedBuilder()
        builder.setTitle(event.getString("digi_version"))
        builder.setColor(Color.MAGENTA)
        builder.setDescription("Version ${Digi.version}")
        builder.setAuthor("Digi", "https://digi.prv.cymru", "https://digi.prv.cymru/digi_v2.png")
        builder.setThumbnail("https://digi.prv.cymru/digi_v2.png")
        event.sendMessage(builder.build())
    }

    private fun printVersion(event: CommandEvent){
        event.sendMessage("Version ${Digi.version}")
    }

    private fun printHelp(event: DiscordCommandEvent){
        if(event.isEmpty()) {
            val builder = EmbedBuilder()
            builder.setColor(baseColor)
            builder.setTitle("Help menu for Digi")
            builder.setDescription("Below you can find some helpful information about how to use the bot. If you have any questions don't hesitate to ask Prv")
            builder.addField(event.getString("header_command_list"), "Use the command `list`", false)
            builder.addField("Help about a specific command", "Use the command `help <command>`. For example `!digi help verb`", false)
            event.sendMessage(builder.build())
        }
        else {
            val command = event.pop()
            val form = event.fetchCommandForm(command)
            if(form != null)
                form.command.printInformation(event)
            else
                event.sendError(event.getString("unknown_command").format(command))
        }
    }

    private fun printHelp(event: CommandEvent){
        event.sendMessage("Some random help message")
    }

    private fun printList(event: CommandEvent){
        val builder = StringBuilder()
        appendCommandsForPlugin(event, builder, this)
        Digi.plugins.minus(BasePlugin).forEach { appendCommandsForPlugin(event, builder, it) }
        appendCommandsForPlugin(event, builder, event.getInterfacePlugin())

        val header = event.formatHeader(event.getString("header_command_list"))
        event.sendMessage("$header\n${event.formatMonospace(builder.toString().trim())}");
    }

    private fun appendCommandsForPlugin(event: CommandEvent, builder: StringBuilder, plugin: Plugin){
        if(plugin.getCommands().isEmpty())
            return
        builder.append("${event.getString(plugin.name)}:\n")
        plugin.getCommands().sortedBy { it.getFormForLanguage(event.outputLanguage) }.forEach {
            builder.append("  %-8s : %s\n".format(
                it.getFormForLanguage(event.outputLanguage),
                it.shortAbout.getString(event.outputLanguage)))
        }
        builder.append("\n")
    }
}