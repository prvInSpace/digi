package cymru.prv.digi.interfaces.telegram

import cymru.prv.digi.Resources
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.TelegramBotsApi
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession


/**
 * Telegram bot for Digi
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
object TelegramBot : TelegramLongPollingBot() {

    override fun getBotUsername(): String {
        return "Digi"
    }

    override fun getBotToken(): String {
        return Resources.getEnvironmentVariable("telegram")
    }

    override fun onUpdateReceived(update: Update) {
        TelegramCommandEvent(this, update).handle()
    }

    init {
        val api = TelegramBotsApi(DefaultBotSession::class.java)
        api.registerBot(this)
    }

}
