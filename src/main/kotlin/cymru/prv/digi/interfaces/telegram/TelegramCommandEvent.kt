package cymru.prv.digi.interfaces.telegram

import cymru.prv.digi.common.Command
import cymru.prv.digi.common.CommandEvent
import cymru.prv.digi.plugins.Plugin
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.exceptions.TelegramApiException


/**
 * Represents a single command event for Telegram
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class TelegramCommandEvent(private val bot: TelegramBot, private val update: Update) :
    CommandEvent(update.message.text
        .replaceFirst("^/digi ".toRegex(), "")
        .replaceFirst("^/start".toRegex(), "help")
        .replaceFirst("^/".toRegex(), "")) {

    companion object TelegramInterfacePlugin: Plugin("Telegram") {
        override fun getCommands(): List<Command> = listOf()
    }

    override fun getInterfacePlugin(): Plugin {
        return TelegramInterfacePlugin
    }

    override fun sendMessage(content: String) {
        val message = SendMessage()
        message.enableMarkdown(true)
        message.chatId = update.message.chatId.toString()
        message.text = content
        message.enableMarkdown(true)
        try {
            bot.execute(message)
        } catch (e: TelegramApiException) {
            e.printStackTrace()
        }
    }

    override fun formatHeader(content: String): String {
        return "**$content**"
    }

    override fun formatMonospace(content: String): String {
        return "```\n$content```"
    }
}
