package cymru.prv.digi.interfaces.discord

import cymru.prv.digi.common.CommandEvent
import cymru.prv.digi.common.Language
import cymru.prv.digi.common.Setting
import cymru.prv.digi.plugins.DiscordSettings
import cymru.prv.digi.plugins.Plugin
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.awt.Color
import java.lang.StringBuilder

/**
 * Represents a single Discord command event.
 * Contains simple overrides to help with basic
 * formatting for Discord.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class DiscordCommandEvent(val event: MessageReceivedEvent, command: String):
    CommandEvent(command) {

    override fun sendError(content: String){
        val builder = EmbedBuilder()
        builder.setColor(Color.RED)
        builder.setTitle(getString("error_title"))
        builder.setDescription(content)
        sendMessage(builder.build())
    }

    override fun getInterfacePlugin(): Plugin {
        return DiscordSettings
    }


    override fun sendMessage(content: String){
        if(content.length < 2000){
            event.channel.sendMessage(content).queue()
            return
        }
        println("Sending message that is ${content.length} long")


        val pattern = Regex("`{3}")
        val parts = content.split("\n\n").toMutableList()
        val builder = StringBuilder()
        var isInMonospace = false
        parts.forEach {
            if(builder.length + it.length > 1990){
                if(isInMonospace)
                    builder.append("\n```\n")
                event.channel.sendMessage(builder.toString()).queue()
                builder.clear()
                if(isInMonospace)
                    builder.append("```\n")
            }
            if(builder.isNotEmpty())
                builder.append("\n\n")
            builder.append(it)

            // Check whether we are in monospace or not
            if(pattern.findAll(it).count() % 2 != 0)
                isInMonospace =! isInMonospace
        }
        if(builder.isNotEmpty())
            event.channel.sendMessage(builder.toString()).queue()
    }
    fun sendMessage(content: MessageEmbed) = event.channel.sendMessage(content).queue()


    override fun formatHeader(content: String): String {
        return "**$content**"
    }

    override fun formatMonospace(content: String): String {
        return "```\n$content```\n"
    }

    override fun getDefaultLanguage(): Language {
        val string = DiscordSettings.getSetting(event, Setting.DefaultLanguage)
            ?: return super.getDefaultLanguage()
        return Language.valueOf(string)
    }

    override fun getDefaultOutputLanguage(): Language {
        val string = DiscordSettings.getSetting(event, Setting.DefaultOutputLanguage)
            ?: return super.getDefaultOutputLanguage()
        return Language.valueOf(string)
    }
}



