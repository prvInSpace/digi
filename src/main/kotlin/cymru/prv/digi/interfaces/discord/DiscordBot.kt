package cymru.prv.digi.interfaces.discord

import cymru.prv.digi.Resources
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.entities.Activity
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

/**
 * @author Preben Vangberg
 */
object DiscordBot: ListenerAdapter() {

    private val prefix = Resources.getEnvironmentVariable("discord_prefix", "!digi")

    val api: JDA by lazy {
        JDABuilder.createDefault(Resources.getEnvironmentVariable("discord_api_key"))
            .addEventListeners(this)
            .setActivity(Activity.listening(prefix))
            .build()
    }

    override fun onMessageReceived(event: MessageReceivedEvent) {
        if(event.author.isBot)
            return

        if(!event.message.contentStripped.startsWith(prefix))
            return

        val text = event.message.contentStripped.replace(prefix, "")
        DiscordCommandEvent(event, text).handle()
    }

}