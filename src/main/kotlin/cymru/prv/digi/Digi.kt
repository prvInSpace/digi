package cymru.prv.digi

import cymru.prv.digi.interfaces.discord.DiscordBot
import cymru.prv.digi.interfaces.telegram.TelegramBot
import cymru.prv.digi.plugins.BasePlugin
import cymru.prv.digi.plugins.dictionary.Dictionary

/**
 * The main object for the bot
 *
 * @author Preben Vangberg
 */
object Digi {

    const val version = "1.0.0"

    val plugins = listOf(BasePlugin, Dictionary)

    val commands = plugins.flatMap { it.getCommands() }.flatMap { it.forms }.associate { it }

    fun start(){
        //commands.forEach { println("${it.key} -> ${it.value.lang}")}
        println("[Discord] Starting Discord API")
        DiscordBot.api.awaitReady()
        println("[Discord] API ready")
        println("[Telegram] Starting Telegram API")
        println("[Telegram] API ready. Name: '${TelegramBot.botUsername}'")
    }

}

fun main() {
    Digi.start()
}
