package cymru.prv.digi

import cymru.prv.digi.common.Language
import cymru.prv.digi.common.Translation
import org.json.JSONObject
import java.io.File
import java.lang.RuntimeException
import java.net.URL

/**
 * A common center for resource related things
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
object Resources {

    private val strings: Map<String, Translation> = fetchTranslations()
    private val environmentVariables: Map<String, String> = fetchEnvironmentVariables()


    /**
     * Fetches the string of the given key and for
     * the given language if it exist. Otherwise
     * it defaults to English.
     */
    fun getString(lang: Language, key: String): String {
        return strings[key]?.getString(lang) ?: key
    }


    /**
     * Fetches the value of the environment variable
     * with the given key
     */
    fun getEnvironmentVariable(key: String): String {
        return environmentVariables[key] ?: throw RuntimeException("Unknown key '$key'")
    }

    fun getEnvironmentVariable(key: String, default: String): String {
        return environmentVariables[key] ?: default
    }


    /**
     * Fetches the environment variables from the settings.json
     * file that should be located in the working directory
     */
    private fun fetchEnvironmentVariables(): Map<String, String> {
        val obj = JSONObject(File("settings.json").readText())
        return obj.keySet().associateWith { obj.getString(it) }.toMap()
    }

    /**
     * Fetches the translations from the JSON file on the
     * website. This file is then compiled into a map
     * where the strings are stored
     */
    private fun fetchTranslations(): Map<String, Translation> {
        val obj = JSONObject(URL("https://digi.prv.cymru/translations.json").readText())
        val internal = JSONObject(javaClass.getResource("/strings.json").readText())
        for(key: String in internal.keySet())
            obj.put(key, internal.getJSONObject(key))

        return obj.keySet().associateWith { Translation(obj.getJSONObject(it)) }
    }
}



