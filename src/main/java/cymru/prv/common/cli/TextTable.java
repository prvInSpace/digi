package cymru.prv.common.cli;

import java.util.*;

public class TextTable {

    private int[] max;
    private int[] lengths;
    private int numCols = 0;
    private List<String[]> rows = new LinkedList<>();
    private Map<Integer, String> specialFormat = new HashMap<>();
    private String[] separators;

    private String formatString;

    public TextTable(String formatString) {
        this.formatString = formatString;
        for (char c : formatString.toCharArray())
            if (c == 'r' || c == 'l')
                numCols++;

        lengths = new int[numCols];
        max = new int[numCols];
        Arrays.fill(max, -1);

        separators = formatString.split("[rl]");
    }

    public void setColFixedWidth(int index, int width){
        setColMinWidth(index, width);
        setColMaxWidth(index, width);
    }

    public void setColMaxWidth(int index, int maxLength) {
        max[index] = maxLength;
    }

    public void setColMinWidth(int index, int minLength) {
        lengths[index] = Math.max(minLength, lengths[index]);
    }

    public void addSplit() {
        rows.add(null);
    }

    public void addRow(String... args) {
        for (int i = 0; i < args.length; ++i) {
            if (lengths[i] < args[i].length()) {
                lengths[i] = args[i].length();
                if(max[i] != -1 && lengths[i] > max[i])
                    lengths[i] = max[i];
            }
        }
        rows.add(args);
    }

    public void addRowWithFormat(String format, String ... args){
        specialFormat.put(rows.size(), format);
        addRow(args);
    }

    private String convertFormat(String format) {
        String converted = format;
        int number = 0;
        int colCount = 1;
        for (int i = 0; i < format.length(); ++i) {
            if(format.charAt(i) >= '0' && format.charAt(i) <= '9'){
                colCount = format.charAt(i) - '0';
            }
            else if (format.charAt(i) == 'r' || format.charAt(i) == 'l') {
                int length = 0;
                for(int c = 0; c < colCount; ++c){
                    length += lengths[number];
                    number++;
                    if(c+1 != colCount)
                        length += separators[number].length();
                }
                if(format.charAt(i) == 'r')
                    converted = converted.replaceFirst("[0-9]*r", "%" + length + "s");
                else
                    converted = converted.replaceFirst("[0-9]*l", "%-" + length + "s");
                colCount = 1;
            }
        }

        return converted;
    }
    public String build() {
        String format = convertFormat(formatString);

        String[] args = new String[lengths.length];
        Arrays.fill(args, "");
        String split = String.format(format, (Object[]) args).replace(' ', '-');

        StringBuilder sb = new StringBuilder();

        boolean isFirst = true;
        for (String[] row : rows) {
            if (row == null)
                sb.append(split).append('\n');
            else {
                String rowFormat = format;
                String columns;
                if(specialFormat.containsKey(rows.indexOf(row)))
                    rowFormat = convertFormat(specialFormat.get(rows.indexOf(row)));

                // Set up the cells
                List<List<String>> cells = new LinkedList<>();
                for (int i = 0; i < numCols; ++i) {
                    List<String> queue = new LinkedList<>();
                    cells.add(queue);
                    if(i >= row.length)
                        queue.add("");
                    else if (max[i] == -1 || row[i].length() <= max[i])
                        queue.add(row[i]);
                    else
                        queue.addAll(Arrays.asList(row[i].split("\\s")));
                }

                // Print row
                int remaining = 1;
                while (remaining != 0) {
                    remaining = 0;
                    String[] strings = new String[numCols];
                    Arrays.fill(strings, "");
                    for (int i = 0; i < numCols; ++i) {
                        while (true) {
                            if (cells.get(i).isEmpty())
                                break;
                            if(max[i] == -1) {
                                strings[i] = cells.get(i).remove(0);
                                break;
                            }
                            if ((strings[i].length() + cells.get(i).get(0).length()) >= max[i]) {
                                if (strings[i].length() == 0 || cells.get(i).get(0).length() >= max[i]) {
                                    String s = cells.get(i).remove(0);
                                    int splitIndex = max[i]-1-strings[i].length();
                                    strings[i] += s.substring(0, splitIndex) + '-';
                                    cells.get(i).add(0, s.substring(splitIndex));
                                }
                                break;
                            }
                            strings[i] += cells.get(i).remove(0) + ' ';
                        }
                        strings[i] = strings[i].trim();
                        remaining = Math.max(remaining, cells.get(i).size());
                    }
                    sb.append(String.format(rowFormat, (Object[]) strings)).append('\n');
                }
            }
        }

        return sb.toString();
    }

}
